CC=g++

CFLAGS=-c -Wall

all:
	g++ -o main main.cpp exception/myException.cpp

clean:
	rm -rf main
