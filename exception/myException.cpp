#include "myException.h"

MyException::MyException() {

}

MyException::MyException(const char * msg)
    :_msg(msg) {

}

MyException::~MyException() {
    
}

const char* MyException::what() const noexcept {
    return _msg;
}