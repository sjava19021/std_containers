#ifndef EXCEPTION
#define EXCEPTION

#include <exception>

class MyException : public std::exception
{
public:
    MyException();
    MyException(const char *);
    ~MyException();
    const char* what() const noexcept override;
private:
    const char *_msg;
};

#endif