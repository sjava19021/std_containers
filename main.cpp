#include "vector/myVector.h"
#include "exception/myException.h"
#include <vector>
#include <iostream>

#define N 10


int main(){
    std::vector<int> sv{};
    
    //std::cout << sv.empty() << std::endl;
    sv.resize(100);
    //sv.resize(0);
    //sv.shrink_to_fit();
    //std::cout << sv.empty() << std::endl;


    Vector<int> v;

    for(int i = 0; i < N; ++i){
        //sv.push_back(i);
        //v.push_back(i);
    }

    std::cout << sv.size() << std::endl;
    std::cout << sv.capacity() << std::endl;

    for(int i = 0; i < sv.size(); ++i){
        std::cout << sv[i] << " ";
    }

    std::cout << "\n===========" << std::endl;

    //std::cout << v.empty() << std::endl;
    v.resize(100);
    //v.resize(0);
    //v.shrink_to_fit();
    //std::cout << v.empty() << std::endl;

    std::cout << v.size() << std::endl;
    std::cout << v.capacity() << std::endl;

    try{
        for(int i = 0; i < v.size(); ++i){
            std::cout << v[i] << " ";
        }
    }
    catch(MyException &ex){
        std::cout << ex.what();
    }

}