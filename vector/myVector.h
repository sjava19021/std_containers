#ifndef VECTOR
#define VECTOR

#include "../exception/myException.h"
#include <initializer_list>

template<class T>
class Vector {
public:
    Vector();
    Vector(std::initializer_list<T>);
    ~Vector();
    int capacity();
    int size();
    void push_back(T);
    int& operator [](int);
    void resize(int);
    bool empty();
    void shrink_to_fit();
private:
    T* _vector;
    int _size;
    int _capacity;
};


template<class T>
Vector<T>::Vector() 
    :_size(0), _capacity(0) {

}  

template<class T>
Vector<T>::Vector(std::initializer_list<T> elems)
{
    _size = elems.size();
    _capacity = _size;
    _vector = new T[_capacity];
    int i = 0;
    for(auto it = elems.begin(); it != elems.end(); ++it){
        _vector[i] = *it;
        i++;
    }
}


template<class T>
Vector<T>::~Vector() {

}

template<class T>
int Vector<T>::capacity() {
    return _capacity;
}

template<class T>
int Vector<T>::size() {
    return _size;
}

template<class T>
void Vector<T>::push_back(T elem) {
    if(_capacity == 0) {
        _vector = new T[1];
        _vector[0] = elem;
        _size = 1;
        _capacity = 1;
    } else {
        if(_size == _capacity) {
            T* _newVector = new T[_capacity * 2];
            for(int i = 0; i < _size; ++i)
                _newVector[i] = _vector[i];
            _newVector[_size] = elem;
            delete[] _vector;
            _vector = _newVector;
            _capacity *= 2;
            _size++;
        } else {
            _vector[_size] = elem;
            _size++;
        }
    }
}

template<class T>
int& Vector<T>::operator[](int indx) {
    if(indx < _size)
        return _vector[indx];
    else
        throw  MyException("\nException: Index out of vector\n");
    
}

template<class T>
void Vector<T>::resize(int newSize){
    if(newSize < 0)
        throw MyException("\nException: Negative size in Vector.resize(size)\n");
    if(newSize == 0){
        _size = 0;
        if(_capacity != 0)
            delete[] _vector;
        _capacity = 0;
    } else if(newSize < _size)
        _size = newSize;
    else if (newSize > _capacity && _capacity != 0) {
        T* _newVector = new T[newSize];
        for(int i = 0; i < _size; ++i)
            _newVector[i] = _vector[i];
        delete[] _vector;
        _vector = _newVector;
        _capacity = newSize;
        _size = newSize;
    }
    else if(_capacity == 0){
        _vector = new T[newSize];
        _capacity = newSize;
        _size = newSize;
    }
    
}

template<class T>
bool Vector<T>::empty() {
    if(_size == 0)
        return true;
    return false;
}

template<class T>
void Vector<T>::shrink_to_fit() {
    if(_size == _capacity)
        return;
    T* newVector = new T[_size];
    for(int i = 0; i < _size; ++i)
        newVector[i] = _vector[i];
    delete[] _vector;
    _vector = newVector;
    _capacity = _size;
}

#endif